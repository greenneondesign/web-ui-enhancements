# Web UI Enancements

There are a number of HTML/Web UI controls that are slightly lacking. This project is meant to overcome those deficiencies in a gracefully degrading manner.

## Features

- Add image preview to file pickers. [Example](examples/FilePickerPreview.html)

## License

[MIT License](LICENSE)

## Author

Tom Egan