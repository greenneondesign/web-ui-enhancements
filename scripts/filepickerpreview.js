function formatFileSize(size) {
	if(size < 1024) {
		return size + 'bytes';
	} else if(size > 1024 && size < 1048576) {
		return (size/1024).toFixed(1) + 'KB';
	} else if(size > 1048576) {
		return (size/1048576).toFixed(1) + 'MB';
	}
}

function updatePreview(previewFilePicker) {
	let preview = previewFilePicker.querySelector('div');
	let input = previewFilePicker.querySelector('input');
	while(preview.firstChild) {
		preview.removeChild(preview.firstChild);
	}

	var files = input.files;
	if(files.length === 0) {
		preview.style.display = "none";
	} else {
		var image = document.createElement('img');
		image.src = window.URL.createObjectURL(files[0]);
		preview.appendChild(image);
		var paragraph = document.createElement('p');
		paragraph.textContent = files[0].name + ' (' + formatFileSize(files[0].size) + ')';
		preview.appendChild(paragraph);
		preview.style.display = "block";
	}
}